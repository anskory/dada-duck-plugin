# Dada-Duck-Plugin

Dada-Duck-Plugin是基于Yunzai-Bot V3版本的插件，提供一些有趣的接口功能，仅适配V3
## 安装与更新
###安装
请将Dada-Duck-Plugin放置在Yunzai-Bot的plugins目录下，重启Yunzai-Bot后即可使用。

推荐使用git进行安装，以方便后续升级。在Yunzai根目录夹打开终端，运行

使用Gitee命令安装
```
git clone https://gitee.com/anskory/dada-duck-plugin.git ./plugins/dada-duck-plugin/
```
###更新
给机器人发送指令`#达达鸭更新`或`#达达鸭强制更新`获取最新版本

## 使用说明

发送指令`#达达鸭帮助`即可获取功能菜单

###其他   
* 最后给个star或者[爱发电](https://afdian.net/a/dadaya)，你的支持是维护本项目的动力~~

* 特别鸣谢 [xiaoyao-cvs-plugin](https://gitee.com/Ctrlcvs/xiaoyao-cvs-plugin) 提供的代码支持

* 特别鸣谢 [Earth-K-plugin](https://gitee.com/SmallK111407/earth-k-plugin) 提供的代码支持
