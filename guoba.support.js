import path from "path";
const _path = process.cwd() + "/plugins/dada-duck-plugin";
import xxCfg from "./model/xxCfg.js";

/**
 *  支持锅巴配置
 */
export function supportGuoba() {
  return {
    pluginInfo: {
      name: "dada-duck-plugin",
      title: "dada-duck-plugin",
      author: "@anskory",
      authorLink: "https://gitee.com/anskory",
      link: "https://gitee.com/anskory/dada-duck-plugin",
      isV3: true,
      isV2: false,
      description: "私有化插件，功能暂无",
      // 显示图标，此为个性化配置
      // 图标可在 https://icon-sets.iconify.design 这里进行搜索
      icon: "mdi:stove",
      // 图标颜色，例：#FF0000 或 rgb(255, 0, 0)
      iconColor: "#d19f56",
      // 如果想要显示成图片，也可以填写图标路径（绝对路径）
      iconPath: path.join(_path, "resources/img/dirt.png"),
    }}}
