import plugin from '../../../lib/plugins/plugin.js'
import {segment} from "oicq";
import fetch from 'node-fetch'


const _path = process.cwd();
let timeout = 20000; //这里是撤回时间，单位毫秒，1000=1秒,0则为不撤回，壁纸星空七濑胡桃默认不撤回，如果需要撤回的话需要手动
let CD = {};// 命令CD
let isR18 = true;//群聊R18默认值
let isR18s = true;//私聊R18默认值
let interval = 10000;//连发模式的间隔时间，默认为10秒，由于图片质量不同导致发送时间不同，实际间隔可能有误差
let num = 3; //默认连发数量为3


//这是群黑名单，在这输入想禁用涩图的群号，逗号隔开，唯有搜索和涩图功能会被禁用
const blacklist = []//例如123456,456789
export class sese extends plugin {
    constructor() {
        super({
            /** 功能名称 */
            name: '涩涩',
            /** 功能描述 */
            dsc: '简单开发示例',
            /** https://oicqjs.github.io/oicq/#events */
            event: 'message',
            /** 优先级，数字越小等级越高 */
            priority: 5000,
            rule: [
                {
                    /** 命令正则匹配 */
                    reg: '^二次元$',
                    /** 执行方法 */
                    fnc: 'ecy'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^涩涩帮助.*$',
                    /** 执行方法 */
                    fnc: 'helpone'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^第二页.*$',
                    /** 执行方法 */
                    fnc: 'helptwo'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^搜索.*$',
                    /** 执行方法 */
                    fnc: 'acgs'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^百合$',
                    /** 执行方法 */
                    fnc: 'baihe'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^P站$',
                    /** 执行方法 */
                    fnc: 'pixiv'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^湿身$',
                    /** 执行方法 */
                    fnc: 'shishen'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^可爱一下$',
                    /** 执行方法 */
                    fnc: 'keaiyixia'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^三次元$',
                    /** 执行方法 */
                    fnc: 'scysese'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^银发$',
                    /** 执行方法 */
                    fnc: 'yinfa'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^兽耳$',
                    /** 执行方法 */
                    fnc: 'shouer'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^壁纸$',
                    /** 执行方法 */
                    fnc: 'suijibizhi'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^星空$',
                    /** 执行方法 */
                    fnc: 'xingkong'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^竖屏壁纸$',
                    /** 执行方法 */
                    fnc: 'spbizhi'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^横屏壁纸$',
                    /** 执行方法 */
                    fnc: 'hpbizhi'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^动漫壁纸$',
                    /** 执行方法 */
                    fnc: 'dmbizhi'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^脚控$',
                    /** 执行方法 */
                    fnc: 'jiaokong'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^来个cos$',
                    /** 执行方法 */
                    fnc: 'coss'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^御姐$',
                    /** 执行方法 */
                    fnc: 'yujie'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^女仆$',
                    /** 执行方法 */
                    fnc: 'nvpu'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^jk制服$',
                    /** 执行方法 */
                    fnc: 'jk'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^泳装$',
                    /** 执行方法 */
                    fnc: 'yongzhuang'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^少女$',
                    /** 执行方法 */
                    fnc: 'shaonv'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^丰满$',
                    /** 执行方法 */
                    fnc: 'fengman'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^黑丝$',
                    /** 执行方法 */
                    fnc: 'heisi'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^网丝$',
                    /** 执行方法 */
                    fnc: 'wangsi'
                },

                {
                    /** 命令正则匹配 */
                    reg: '^旗袍$',
                    /** 执行方法 */
                    fnc: 'qipao'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^我的世界$',
                    /** 执行方法 */
                    fnc: 'wdsj'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^赛马娘$',
                    /** 执行方法 */
                    fnc: 'smn'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^东京喰种$',
                    /** 执行方法 */
                    fnc: 'djssg'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^fate$',
                    /** 执行方法 */
                    fnc: 'fate'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^为美好世界献上祝福$',
                    /** 执行方法 */
                    fnc: 'wmhsjxszf'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^超电磁炮$',
                    /** 执行方法 */
                    fnc: 'mkxdcdcp'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^原神|原批|op$',
                    /** 执行方法 */
                    fnc: 'yuanshen'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^神奇宝贝$',
                    /** 执行方法 */
                    fnc: 'sqbb'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^龙珠$',
                    /** 执行方法 */
                    fnc: 'lz'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^罪恶王冠$',
                    /** 执行方法 */
                    fnc: 'zewg'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^鬼灭之刃$',
                    /** 执行方法 */
                    fnc: 'gmzr'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^火影忍者$',
                    /** 执行方法 */
                    fnc: 'hyrz'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^海贼王$',
                    /** 执行方法 */
                    fnc: 'hzw'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^进击的巨人$',
                    /** 执行方法 */
                    fnc: 'jjdjr'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^从零开始的异世界生活$',
                    /** 执行方法 */
                    fnc: 'clksdysjsh'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^刀剑神域$',
                    /** 执行方法 */
                    fnc: 'djsy'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^钢之炼金术师$',
                    /** 执行方法 */
                    fnc: 'gzljss'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^妖精的尾巴$',
                    /** 执行方法 */
                    fnc: 'yjdwb'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^缘之空$',
                    /** 执行方法 */
                    fnc: 'yzk'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^物语$',
                    /** 执行方法 */
                    fnc: 'wuyu'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^少女前线$',
                    /** 执行方法 */
                    fnc: 'snqx'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^明日方舟$',
                    /** 执行方法 */
                    fnc: 'mrfz'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^重装战姬$',
                    /** 执行方法 */
                    fnc: 'zzzj'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^CG$',
                    /** 执行方法 */
                    fnc: 'cg'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^守望先锋$',
                    /** 执行方法 */
                    fnc: 'swxf'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^王者荣耀$',
                    /** 执行方法 */
                    fnc: 'wzry'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^开启R18|关闭R18|开启r18|关闭r18$',
                    /** 执行方法 */
                    fnc: 'R18Switch'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^设置撤回.*$',
                    /** 执行方法 */
                    fnc: 'chehui'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^设置连发间隔.*$',
                    /** 执行方法 */
                    fnc: 'setlnterval'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^设置连发数量.*$',
                    /** 执行方法 */
                    fnc: 'sztNum'
                },
                {
                    /** 命令正则匹配 */
                    reg: '^开启连发|关闭连发$',
                    /** 执行方法 */
                    fnc: 'intervalAcg'
                },
            ]
        })
    }

    async ecy(e) {

        //执行的逻辑功能
        let url = `https://api.ilolicon.com/sese-2`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async helpone(e) {
        await this.reply(`已收藏以下好康的：\n会撤回：\n【二次元】【三次元】\n【jk制服】【来个cos】\n【脚控】【银发】\n【兽耳】【女仆】【少女】\n【御姐】【丰满】【湿身】\n【泳装】【旗袍】【P站】\n【黑丝】【网丝】【白丝】\n【可爱一下】【搜索xxx】\n不会撤回：\n【壁纸】【星空】【竖屏壁纸】【横屏壁纸】【动漫壁纸】\n【我的世界】\n\n注意节制哦(˵¯͒〰¯͒˵)\n友情提示没有#哦\n有第二页噢\n指令：第二页`)
    }

    async helptwo(e) {
        await this.reply(`已收藏以下好康的：\n【赛马娘】【东京喰种】【fate】\n【为美好世界献上祝福】\n【超电磁炮】【原神】\n【神奇宝贝】【龙珠】\n【罪恶王冠】【鬼灭之刃】\n【火影忍者】【海贼王】\n【进击的巨人】\n【从零开始的异世界生活】\n【刀剑神域】\n【钢之炼金术师】\n【妖精的尾巴】【缘之空】\n【物语】【少女前线】\n【明日方舟】【CG】\n【守望先锋】【王者荣耀】\n【重装战姬】\n\n小撸怡情，大撸伤身，酌情而撸。`)
    }

    async acgs(e) {
        if (e.isGroup) { //群聊
            for (let i = 0; i < blacklist.length; i++) {
                if (e.group.group_id == blacklist[i]) {
                    e.reply("本群不可以色色！");
                    return true;
                }
            }
            if (CD[e.user_id] && !e.isMaster) {
                e.reply("每10秒只能冲一次哦！");//更改完冷却时间记得更改这里的时间.
                return true;
            }
            CD[e.user_id] = true;
            CD[e.user_id] = setTimeout(() => {
                if (CD[e.user_id]) {
                    delete CD[e.user_id];
                }
            }, 10000);//这里是冷却时间，单位毫秒.
            e.reply(`正在搜图...`);
            let keyword = e.msg.replace("#", "");
            keyword = keyword.replace("搜索", "");
            let url = '';
            if (!isR18) {
                url = `https://api.lolicon.app/setu/v2?tag=${keyword}&proxy=i.pixiv.re&r18=0`;//setu接口地址，把这里的18=0改成18=1可以发送r18图片，18=2则为混合图片.
            } else {
                url = `https://api.lolicon.app/setu/v2?tag=${keyword}&proxy=i.pixiv.re&r18=1`;
            }
            const response = await fetch(url); //调用接口获取数据
            let res = await response.json(); //结果json字符串转对象
            let imgurl = res.data[0].urls.original;
            if (res.data.length == 0) {
                e.reply("暂时没有搜到哦！换个关键词试试吧！");
                return true;
            }
            let TagNumber = res.data[0].tags.length;
            let Atags;
            let Btags;
            let qwq = 0;
            while (TagNumber--) {
                Atags = res.data[0].tags[TagNumber];
                if (qwq == 0) {
                    Btags = "";
                }
                Btags = Btags + " " + Atags;
                qwq++;
            }
            let msg;
            let pid = res.data[0].pid;
            //最后回复消息
            msg = [
                "标题：",
                res.data[0].title,
                "\n作者：",
                res.data[0].author,
                "\n关键词：",
                Btags,
                segment.image(res.data[0].urls.original),
            ];
            //发送消息
            let msgRes = await e.reply(msg);
            if (timeout != 0 && msgRes && msgRes.message_id) {
                let target = e.group;
                setTimeout(() => {
                    target.recallMsg(msgRes.message_id);
                }, timeout);
            }
            return true; //返回true 阻挡消息不再往下
        } else {  //私聊
            if (CD[e.user_id] && !e.isMaster) {
                e.reply("每1分钟只能冲一次哦！");//更改完冷却时间记得更改这里的时间.
                return true;
            }
            CD[e.user_id] = true;
            CD[e.user_id] = setTimeout(() => {
                if (CD[e.user_id]) {
                    delete CD[e.user_id];
                }
            }, 60000);//这里是冷却时间，单位毫秒.
            e.reply(`正在搜图...`);
            let keyword = e.msg.replace("#", "");
            keyword = keyword.replace("搜索", "");
            let url = '';
            if (!isR18s) {
                url = `https://api.lolicon.app/setu/v2?tag=${keyword}&proxy=i.pixiv.re&r18=0`;//setu接口地址，把这里的18=0改成18=1可以发送r18图片，18=2则为混合图片.
            } else {
                url = `https://api.lolicon.app/setu/v2?tag=${keyword}&proxy=i.pixiv.re&r18=1`;
            }
            const response = await fetch(url); //调用接口获取数据
            let res = await response.json(); //结果json字符串转对象
            let imgurl = res.data[0].urls.original;
            if (res.data.length == 0) {
                e.reply("暂时没有搜到哦！换个关键词试试吧！");
                return true;
            }
            let TagNumber = res.data[0].tags.length;
            let Atags;
            let Btags;
            let qwq = 0;
            while (TagNumber--) {
                Atags = res.data[0].tags[TagNumber];
                if (qwq == 0) {
                    Btags = "";
                }
                Btags = Btags + " " + Atags;
                qwq++;
            }
            let msg;
            let pid = res.data[0].pid;
            //最后回复消息
            msg = [
                "标题：",
                res.data[0].title,
                "\n作者：",
                res.data[0].author,
                "\n关键词：",
                Btags,
                segment.image(res.data[0].urls.original),
            ];
            //发送消息
            e.reply(msg);
            return true; //返回true 阻挡消息不再往下
        }
    }

    async pixiv(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('P站系列4')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async shishen(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('P站系列2')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async keaiyixia(e) {

        //执行的逻辑功能
        let url = `https://api.ilolicon.com/sese`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async scysese(e) {

        //执行的逻辑功能
        let url = `https://ovooa.com/API/meizi/api.php?type=image`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async yinfa(e) {

        //执行的逻辑功能
        let url = `https://dev.iw233.cn/api.php?sort=yin`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async shouer(e) {

        //执行的逻辑功能
        //let url = `https://iw233.cn/api.php?sort=cat`;
        //let url = `http://api.iw233.cn/api.php?sort=cat`;
        //let url = `http://ap1.iw233.cn/api.php?sort=cat`;
        let url = `https://dev.iw233.cn/api.php?sort=cat`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }


    async xingkong(e) {

        //执行的逻辑功能
        let url = `https://dev.iw233.cn/api.php?sort=xing`;
        let msg = [
            segment.image(url),
        ]
        let timeout = 0; //0表示不撤回，单位毫秒  //执行的逻辑功能
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async suijibizhi(e) {

        //执行的逻辑功能
        let url = `https://dev.iw233.cn/api.php?sort=random`;
        let msg = [
            segment.image(url),
        ]
        let timeout = 0; //0表示不撤回，单位毫秒  //执行的逻辑功能
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async spbizhi(e) {

        //执行的逻辑功能
        let url = `https://dev.iw233.cn/api.php?sort=mp`;
        let msg = [
            segment.image(url),
        ]
        let timeout = 0; //0表示不撤回，单位毫秒  //执行的逻辑功能
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async hpbizhi(e) {

        //执行的逻辑功能
        let url = `https://dev.iw233.cn/api.php?sort=pc`;
        let msg = [
            segment.image(url),
        ]
        let timeout = 0; //0表示不撤回，单位毫秒  //执行的逻辑功能
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async dmbizhi(e) {

        //执行的逻辑功能
        let url = `https://iw233.cn/api.php?sort=top`;
        let msg = [
            segment.image(url),
        ]
        let timeout = 0; //0表示不撤回，单位毫秒  //执行的逻辑功能
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }
    async jiaokong(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('脚控')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async coss(e) {

        //执行的逻辑功能
        let url = `https://imgapi.cn/cos2.php?return=img`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async yujie(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('御姐')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async nvpu(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('女仆')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async jk(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('jk制服')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async yongzhuang(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('夏日泳装')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async shaonv(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('少女')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async fengman(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('丰满微胖')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async baisi(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('白丝')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async heisi(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('黑丝')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async wangsi(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('网丝')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async qipao(e) {

        //执行的逻辑功能
        let url = `https://api.uomg.com/api/rand.img4?sort=${encodeURI('旗袍')}&format=images`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }

        return true; //返回true 阻挡消息不再往下
    }

    async wdsj(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('我的世界系列1')}`;
        let msg = [
            segment.image(url),
        ]
        let timeout = 0; //0表示不撤回，单位毫秒  //执行的逻辑功能
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async smn(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('赛马娘')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async djssg(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('东京食尸鬼')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }


    async fate(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=Fate`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }


    async wmhsjxszf(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('为美好世界献上祝福')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }


    async mkxdcdcp(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('某科学的超电磁炮')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }


    async yuanshen(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('原神')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }


    async sqbb(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('神奇宝贝')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async lz(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('龙珠')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async zewg(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('罪恶王冠')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async gmzr(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('鬼灭之刃')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async hyrz(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('火影忍者')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async hzw(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('海贼王')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async jjdjr(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('进击的巨人')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async clksdysjsh(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('从零开始的异世界生活')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async djsy(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('刀剑神域')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async gzljss(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('钢之炼金术师竖屏系列1')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async yjdwb(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('妖精的尾巴')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async yzk(e) {
        let url = `https://api.r10086.com/img-api.php?zsy=${encodeURI('缘之空')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async wuyu(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('物语系列2')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async snqx(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('少女前线1')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async mrfz(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('明日方舟1')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async zzzj(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('重装战姬1')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async cg(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('CG系列3')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async swxf(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('守望先锋')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }

    async wzry(e) {
        let url = `https://api.r10086.com/img-api.php?type=${encodeURI('王者荣耀')}`;
        let msg = [
            segment.image(url),
        ]
        let msgRes = await e.reply(msg);
        if (timeout != 0 && msgRes && msgRes.message_id) {
            let target = null;
            if (e.isGroup) {
                target = e.group;
            } else {
                target = e.friend;
            }
            if (target != null) {
                setTimeout(() => {
                        target.recallMsg(msgRes.message_id);
                        target.recallMsg(e.message_id);
                    },
                    timeout);
            }
        }
        return true; //返回true 阻挡消息不再往下
    }


    async R18Switch(e) {
        let onoff;
        if (e.msg.indexOf("R18") > -1) {
            onoff = e.msg.replace("R18", "");
        } else if (e.msg.indexOf("r18") > -1) {
            onoff = e.msg.replace("r18", "");
        }
        if (e.isGroup) {
            if (onoff == '开启' && e.isMaster) {
                e.reply(`群聊R18已开启`);
                isR18 = true
            } else if (onoff == '关闭' && e.isMaster) {
                e.reply(`群聊R18已关闭`);
                isR18 = false
            } else if (!e.isMaster) {
                e.reply(`只有主人可以命令鸭鸭哦~`);
            }
        } else {
            if (onoff == '开启' && e.isMaster) {
                e.reply(`私聊R18已开启`);
                isR18s = true
            } else if (onoff == '关闭' && e.isMaster) {
                e.reply(`私聊R18已关闭`);
                isR18s = false
            } else if (!e.isMaster) {
                e.reply(`只有主人可以命令鸭鸭哦~`);
            }
        }
    }

    async chehui(e) {
        let onoff;
        if (e.msg.indexOf("设定") > -1) {
            onoff = e.msg.replace("设定撤回", "");
        } else if (e.msg.indexOf("设置") > -1) {
            onoff = e.msg.replace("设置撤回", "");
        }
        if (e.msg.indexOf("时间") > -1) {
            onoff = e.msg.replace("时间", "");
        }
        if (onoff == '关闭' && e.isMaster) {
            e.reply(`自动撤回已关闭`);
            timeout = 0;
        } else if (e.isMaster) {
            onoff = parseInt(Math.abs(onoff))
            onoff = Math.trunc(onoff)
            if (typeof onoff === 'number') {
                timeout = onoff * 1000;
                e.reply(`自动撤回已设定为` + onoff + `秒`);
            } else {
                e.reply(`输入的指令有误`);
            }
        } else if (!e.isMaster) {
            e.reply(`只有主人可以命令鸭鸭哦~`);
        }
    }

    async setInterval(e) {
        let onoff;
        if (e.msg.indexOf("设定") > -1) {
            onoff = e.msg.replace("设定连发间隔", "");
        } else if (e.msg.indexOf("设置") > -1) {
            onoff = e.msg.replace("设置连发间隔", "");
        }
        if (onoff == '关闭' && e.isMaster) {
            e.reply(`连发间隔已归零`);
            interval = 0;
        } else if (e.isMaster) {
            onoff = parseInt(Math.abs(onoff))
            onoff = Math.trunc(onoff)
            if (typeof onoff === 'number') {
                interval = onoff * 1000;
                e.reply(`连发间隔已设定为` + onoff + `秒`);
            } else {
                e.reply(`输入的指令有误`);
            }
        } else if (!e.isMaster) {
            e.reply(`只有主人可以命令鸭鸭哦~`);
        }
    }

    async setNum(e) {
        let onoff;
        if (e.msg.indexOf("设定") > -1) {
            onoff = e.msg.replace("设定连发数量", "");
        } else if (e.msg.indexOf("设置") > -1) {
            onoff = e.msg.replace("设置连发数量", "");
        }
        if (onoff == '无限' && e.isMaster) {
            e.reply(`连发数量已设定为无限`);
            num = Infinity;
        } else if (e.isMaster) {
            onoff = parseInt(Math.abs(onoff))
            onoff = Math.trunc(onoff)
            if (typeof onoff === 'number') {
                if (onoff <= 10) {
                    num = onoff;
                    e.reply(`连发数量已设定为` + onoff);
                } else {
                    num = 10;
                    e.reply(`连发数量最大为10，已设定为10`);
                }
            } else {
                e.reply(`输入的指令有误`);
            }
        } else if (!e.isMaster) {
            e.reply(`只有主人可以命令鸭鸭哦~`);
        }
    }

    async intervalAcg(e) {
        let onoff;
        let url;
        let msg;

        function timer(n) {
            let i = 0;
            time();

            function time() {
                if (i < n) {
                    e.reply(msg);
                    i++;
                    setTimeout(time, interval);
                } else {
                    return true;
                }
            }
        }

        onoff = e.msg.replace("连发", "");
        if (e.isGroup) {
            if ((onoff == '开启' && e.isMaster && num == Infinity) || (onoff == '开启' && num !== Infinity)) {
                if (!isR18) {
                    url = `http://iw233.fgimax2.fgnwctvip.com/API/Ghs.php`;
                    msg = [
                        segment.image(url),
                    ];
                } else {
                    url = `https://api.lolicon.app/setu/v2?proxy=i.pixiv.re&r18=1`;
                    const response = await fetch(url); //调用接口获取数据
                    let res = await response.json(); //结果json字符串转对象
                    //最后回复消息
                    msg = [
                        segment.image(res.data[0].urls.original),
                    ];
                }
                if (num == 0) {
                    e.reply(`请先设置连发数量`);
                    return true;
                } else if (num == Infinity) {
                    e.reply(`连发模式已开启，请注意开启无限连发时，连发模式无法关闭，想要关闭建议重启！`);
                } else {
                    e.reply(`连发模式已开启，本次共发送` + num + `张图片`);
                }
                timer(num);
                return true;
            } else if (onoff == '关闭' && e.isMaster) {
                e.reply(`连发模式无法关闭，若为无限连发，建议重启`);
            } else {
                e.reply(`只有主人才可以命令小七哦~`);
            }
        } else {
            e.reply(`此功能只有群聊可以使用哦！`);
        }
    }
}
